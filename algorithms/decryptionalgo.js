const express = require('express')
const router = express.Router()
const crypto = require('crypto');
const NodeRSA = require('node-rsa');
const path = require("path");
const fs = require("fs");


function getprivatekey(relativeOrAbsolutePathtoPrivateKey){
    var absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey);
    var privateKey = fs.readFileSync(absolutePath, "utf8");
    return privateKey;

}

var decryptStringWithRsaPrivateKey = function(toDecrypt, relativeOrAbsolutePathtoPrivateKey) {
    var privateKey = getprivatekey(relativeOrAbsolutePathtoPrivateKey)
    var buffer = Buffer.from(toDecrypt, "base64");
    var decrypted = crypto.privateDecrypt(privateKey, buffer);
    return decrypted.toString("utf8");
};

function shadecryption(encdata,rsadecryption){
    let hash = crypto
        .createHash('sha512')
        .update(encdata+rsadecryption)
        .digest('hex');
    return hash;
}

// function aesdecryption(rsadecryption,encdata){
//     console.log("IN aesdecryption")
//     let iv = crypto.randomBytes(16);
//     // console.log("iv",iv)
//     let keyone = rsadecryption.substring(0,32);
//     // console.log("keyone",keyone)
//     let decipher = crypto.createDecipheriv('aes-256-cbc',keyone,iv);
//     // console.log("decipher",decipher);
//     // decipher.setAutoPadding(false);
//     let decrypted = decipher.update(encdata,'hex','base64');
//     console.log(decrypted);
//     decrypted += decipher.final('base64');
//     console.log('decrypted',decrypted)
//     return decrypted;
// }

function aesdecryption(text) {
    console.log(text);
    let keybuffer = Buffer.from(text.Aeskey, 'hex')
    // console.log("in decrypt key",key)
    console.log("iv before buffer",text.iv)
    // let iv = crypto.randomBytes(32);
    let iv = Buffer.from(text.iv, 'hex');
    console.log("iv after buffer",iv)
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    // console.log("key befor decipher",key)
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(keybuffer), iv);
    //  decipher.setAutoPadding(false);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

module.exports = {
    decryptStringWithRsaPrivateKey,getprivatekey,decryptStringWithRsaPrivateKey,shadecryption,aesdecryption
}