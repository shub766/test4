const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
console.log("first iv",iv)
// var crypto = require("crypto");
var path = require("path");
var fs = require("fs");

function encrypt(text) {
    console.log("encrypt key",key)
    console.log("encrypt IV",iv)
 let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
 let encrypted = cipher.update(text);
 encrypted = Buffer.concat([encrypted, cipher.final()]);
 return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex'),aeskey:key };
}

var encryptStringWithRsaPublicKey = function(toEncrypt, relativeOrAbsolutePathToPublicKey) {
    var absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
    var publicKey = fs.readFileSync(absolutePath, "utf8");
    var buffer = Buffer.from(toEncrypt);
    var encrypted = crypto.publicEncrypt(publicKey, buffer);
    return encrypted.toString("base64");
};

function shaencryption(encdata,aeskey){
    let hash = crypto
        .createHash('sha512')
        .update(encdata+aeskey)
        .digest('hex');
    return hash;
}

// function decrypt(text) {
// console.log(key)
//  let iv = Buffer.from(text.iv, 'hex');
//  let encryptedText = Buffer.from(text.encryptedData, 'hex');
//  let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
// //  decipher.setAutoPadding(false);
//  let decrypted = decipher.update(encryptedText);
//  decrypted = Buffer.concat([decrypted, decipher.final()]);
//  return decrypted.toString();
// }

function decrypt(text) {
    console.log("in decrypt key",key)
    console.log("iv before buffer",text.iv)
    let iv = Buffer.from(text.iv, 'hex');
    console.log("iv after buffer",iv)
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    console.log("key befor decipher",key)
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
    //  decipher.setAutoPadding(false);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

var hw = encrypt(":)")
console.log(hw)
const aeskey = hw.aeskey;
const encdata = hw.encryptedData;
obj = {
    iv: 'fb007daeadf79295465226cd08bc867c',
    encryptedData: '435b5006d1ac3439a0b805277dac5557',
    // aeskey: decrypted.toString('hex').substring(0,32),
  }
console.log("Aes decryption",decrypt(hw));
console.log(aeskey);

let filepath = path.join(__dirname,'.//certificates/credence_public.key')
// console.log(filepath)
let rsaencryption = encryptStringWithRsaPublicKey(aeskey,filepath)
console.log("RSAENCRYPTION ",rsaencryption)

const hash = shaencryption(encdata,aeskey)
console.log("HASH ",hash);